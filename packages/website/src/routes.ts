/**
 * List of routes corresponding to pages (not assets).
 */
export const ROUTES: readonly string[] = [
  "/",
  "/donate",
  "/forum",
  "/forum/sections/:section_id",
  "/forum/sections/:section_id/new",
  "/forum/threads/:thread_id",
  "/forum/threads/:thread_id/reply",
  "/games",
  "/legal",
  "/login",
  "/register",
  "/register/email",
  "/register/username",
  "/settings",
  "/users/:user_id"
];
