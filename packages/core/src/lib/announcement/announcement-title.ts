/**
 * An announcement title
 */
export type AnnouncementTitle = string;
