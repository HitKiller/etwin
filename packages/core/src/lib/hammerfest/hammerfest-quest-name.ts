import { $Ucs2String, Ucs2StringType } from "kryo/lib/ucs2-string.js";

export type HammerfestQuestName = string;

export const $HammerfestQuestName: Ucs2StringType = $Ucs2String;
