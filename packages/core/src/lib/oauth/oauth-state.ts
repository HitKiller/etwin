import { $Ucs2String, Ucs2StringType } from "kryo/lib/ucs2-string.js";

export type OauthState = string;

export const $OauthState: Ucs2StringType = $Ucs2String;
