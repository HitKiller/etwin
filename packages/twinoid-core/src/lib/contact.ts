import { User } from "./user.js";

export interface Contact {
  friend: boolean;
  user: User;
}
