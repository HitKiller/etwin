export interface DateDay {
  year: number;
  // 1-indexed month
  month: number;
  // 1-indexed day of the month
  day: number;
}
