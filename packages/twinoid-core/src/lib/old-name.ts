export interface OldName {
  name: string;
  until: Date;
}
